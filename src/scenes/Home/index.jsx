import React from 'react';
import { Provider } from 'react-redux';
import Chat from './../../containers/Chat';
import store from './../../store';

const Home = () => (
    <Provider store={store}>
        <Chat />
    </Provider>
);

export default Home;