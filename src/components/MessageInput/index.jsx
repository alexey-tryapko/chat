import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, FormGroup } from 'semantic-ui-react';
import styles from './styles.module.css';

class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            body: '',
        };
        this.handleAddMessage = this.handleAddMessage.bind(this);
    }

    handleAddMessage() {
        const { body } = this.state;
        if (!body) {
            return;
        }
        this.props.addMessage({ body });
        this.setState({ body: '' });
    }

    render() {
        const { body } = this.state;

        return (
            <Form reply onSubmit={this.handleAddMessage}className={styles.formWrapper}>
                <FormGroup widths='equal'>
                    <Form.TextArea
                        value={body}
                        placeholder="Type a message..."
                        onChange={ev => this.setState({ body: ev.target.value })}
                        className={styles.messageInput}
                    />
                    <Button type="submit" className={styles.btnSent}>SEND</Button>
                </FormGroup>
            </Form>
        );
    }
}

MessageInput.propTypes = {
    addMessage: PropTypes.func.isRequired,
};

export default MessageInput;