import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import styles from './styles.module.css';

const Separator = props => {
    const { date } = props;
    const label = moment(date).fromNow();
    return (
        <p className={styles.separator}>{label}</p>
    );
};

Separator.propTypes = {
    date: PropTypes.string.isRequired,
};

export default Separator;