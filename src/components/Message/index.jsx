import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Comment as MessageUI, Icon } from 'semantic-ui-react'
import styles from './styles.module.css';

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isHovered: false,
        }
        this.creatorActions= this.creatorActions.bind(this);
    }

    creatorActions({ id, toggleEditedMessage, deleteMessage, likeCount }) {
        return (
            <MessageUI.Actions>
                <MessageUI.Action>
                    <Icon name="thumbs up" />
                    {likeCount || ''}
                </MessageUI.Action>
                {
                    this.state.isHovered
                    && (
                        <div className={styles.creatorsActions}>
                            <MessageUI.Action onClick={() => toggleEditedMessage(id)}>
                                <Icon name="edit outline" />
                                Edit
                            </MessageUI.Action>
                            <MessageUI.Action onClick={() => deleteMessage(id)}>
                                <Icon name="trash alternate" />
                                Delete
                            </MessageUI.Action>
                        </div>
                    )
                }
            </MessageUI.Actions>);
    }

    defaultActions({ id, reactMessage, likeCount }) {
        return (
            <MessageUI.Actions>
                <MessageUI.Action onClick={() => reactMessage(id)}>
                    <Icon name="thumbs up" />
                    {likeCount || ''}
                </MessageUI.Action>
            </MessageUI.Actions>
        )
    }

    toggleHoverState() {
        this.setState({
            isHovered: !this.state.isHovered,
        });
    };

    render() {
        const { id, user, avatar, created_at, message, likes = [] } = this.props.message;
        const { reactMessage, deleteMessage, toggleEditedMessage, isCreator } = this.props;
        const date = moment(created_at).fromNow();
        const actionArgs = { id, toggleEditedMessage, reactMessage, deleteMessage, likeCount: likes.length }
        return (
            <MessageUI.Group className={isCreator ? styles.creatorMessage : ''}>
                <MessageUI
                    onMouseEnter={() => this.toggleHoverState()}
                    onMouseLeave={() => this.toggleHoverState()}
                >
                    {
                        !isCreator && (
                            <MessageUI.Avatar src={avatar} />
                        )
                    }
                    <MessageUI.Content>
                        {
                            !isCreator && (
                                <MessageUI.Author as='a'>{user}</MessageUI.Author>
                            )
                        }
                        <MessageUI.Metadata>{date}</MessageUI.Metadata>
                        <MessageUI.Text>{message}</MessageUI.Text>
                        {
                            isCreator ?
                                this.creatorActions(actionArgs) :
                                this.defaultActions(actionArgs)
                        }

                    </MessageUI.Content>
                </MessageUI>
            </MessageUI.Group>
        );
    }
}

Message.propTypes = {
    message: PropTypes.objectOf(PropTypes.any).isRequired,
    isCreator: PropTypes.bool.isRequired,
    reactMessage: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    toggleEditedMessage: PropTypes.func.isRequired,
};
export default Message;