import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import styles from './styles.module.css';

class Header extends React.Component {
    render() {
        const { chatName, numberOfParticipants, numberOfMessages, lastMessageTime } = this.props;
        const lastMessageTimeFormated = moment(lastMessageTime, 'YYYY-MM-DD HH:mm:ss').fromNow();

        return (
            <header className={styles.mainHeader}>
                <h1 className={styles.chatName}>{chatName}</h1>
                <ul className={styles.chatInfoList}>
                    <li className={styles.chatInfoItem}>{numberOfParticipants} members</li>
                    <li className={styles.chatInfoItem}>{numberOfMessages} messages</li>
                    <li className={styles.chatInfoItem}>last message {lastMessageTimeFormated}</li>
                </ul>
            </header >
        );
    }
}

Header.propTypes = {
    chatName: PropTypes.string.isRequired,
    numberOfParticipants: PropTypes.number.isRequired,
    numberOfMessages: PropTypes.number.isRequired,
    lastMessageTime: PropTypes.string.isRequired
};

export default Header;