import React from 'react';
import { render } from 'react-dom';
import Home from './scenes/Home';

import './styles/reset.css';
import 'semantic-ui-css/semantic.min.css'
import './styles/common.css';

const target = document.getElementById('root');
render(<Home />, target);
