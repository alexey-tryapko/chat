import * as messageService from './../../services/messageService';
import {
    SET_ALL_MESSAGES,
    ADD_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE,
    SET_EDITED_MESSAGE,
    SET_IS_LOADING,

} from './actionTypes';

const setMessagesAction = messages => ({
    type: SET_ALL_MESSAGES,
    messages
});

const addMessageAction = message => ({
    type: ADD_MESSAGE,
    message
});

const editMessageAction = message => ({
    type: EDIT_MESSAGE,
    message
});

const deleteMessageAction = messageId => ({
    type: DELETE_MESSAGE,
    messageId
});

const setEditedMessageAction = message => ({
    type: SET_EDITED_MESSAGE,
    message
});

const setIsLoadingAction = isLoading => ({
    type: SET_IS_LOADING,
    isLoading
});

export const loadMessages = () => async (dispatch) => {
    dispatch(setIsLoadingAction(true));
    const messages = await messageService.getAllMessages();
    dispatch(setMessagesAction(messages));
    dispatch(setIsLoadingAction(false));
};

export const addMessage = message => async (dispatch, getRootState) => {
    const { chat: { currentUser } } = getRootState();
    const newMessage = await messageService.addMessage(message, currentUser);
    dispatch(addMessageAction(newMessage));
};

export const editMessage = message => async (dispatch, getRootState) => {
    const { chat: { messages } } = getRootState();
    const editedMessage = await messageService.editMessage(message, messages);
    dispatch(editMessageAction(editedMessage));
};

export const deleteMessage = messageId => async (dispatch) => {
    const res = await messageService.deleteMessage(messageId);
    if (res) dispatch(deleteMessageAction(messageId));
};

export const likeMessage = messageId => async (dispatch, getRootState) => {
    const { chat: { messages, currentUser: {
        id: currentUserId
    } } } = getRootState();
    const updatedMessage = await messageService.likeMessage(messageId, messages, currentUserId);

    const updated = messages.map(message => ((message.id !== messageId) ? message : updatedMessage));

    dispatch(setMessagesAction(updated));
};

export const toggleEditedMessage = messageId => async (dispatch, getRootState) => {
    const { chat: { messages } } = getRootState();
    const message = messageId ? await messageService.getMessage(messageId, messages) : undefined;
    dispatch(setEditedMessageAction(message));
};