import {
    SET_ALL_MESSAGES,
    ADD_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE,
    SET_EDITED_MESSAGE,
    SET_IS_LOADING,

} from './actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case SET_ALL_MESSAGES:
            return {
                ...state,
                messages: action.messages,
            };
        case ADD_MESSAGE:
            return {
                ...state,
                messages: [...state.messages, action.message]
            };
        case EDIT_MESSAGE:
            return {
                ...state,
                messages: state.messages.map(message => (
                    message.id === action.message.id ? action.message : message
                ))
            };
        case DELETE_MESSAGE:
            return {
                ...state,
                messages: state.messages.filter(message => message.id !== action.messageId)
            };
        case SET_EDITED_MESSAGE:
            return {
                ...state,
                editedMessage: action.message
            };
        case SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.isLoading
            };
        default:
            return state;
    };
};