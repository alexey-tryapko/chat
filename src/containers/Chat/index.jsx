import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Header from './../../components/Header';
import MessageList from './../../components/MessageList';
import MessageInput from './../../components/MessageInput';
import EditMessage from './../../components/EditMessage';
import Spinner from './../../components/Spinner';
import moment from 'moment';

import {
    loadMessages,
    likeMessage,
    toggleEditedMessage,
    addMessage,
    editMessage,
    deleteMessage,
} from './actions'
import styles from './styles.module.css';

class Chat extends React.Component {
    
    getParticipantsNumber(messages) {
        const participants = new Set(messages.map(item => item.user));
        return participants.size;
    };

    getLastMessageTime(messages) {
        const sortedArray = messages.sort((a, b) => (
            moment(a.created_at).format('YYYY-MM-DD HH:mm:ss') - moment(b.created_at).format('YYYY-MM-DD HH:mm:ss')))
        return sortedArray[sortedArray.length - 1]["created_at"];
    };

    componentDidMount() {
        this.props.loadMessages();
        document.addEventListener('keydown', e => (e.keyCode === 38 && this.handleArrowUpPress()))
    }

    handleArrowUpPress() {
        const { messages, currentUser: { user }, toggleEditedMessage } = this.props;
        const userMessages = messages.filter(message => message.user === user);
        if (userMessages.length) toggleEditedMessage(userMessages.pop().id);
    };

    render() {
        const {
            isLoading,
            messages,
            editedMessage,
            ...props
        } = this.props;

        return (
            <div className={styles.mainWrapper}>
                {
                    isLoading
                        ? <Spinner />
                        : (
                            <div className={styles.contentWrapper}>
                                <Header
                                    chatName={props.chatName}
                                    numberOfParticipants={this.getParticipantsNumber(messages)}
                                    numberOfMessages={messages.length}
                                    lastMessageTime={this.getLastMessageTime(messages)}
                                />
                                <MessageList
                                    messages={messages}
                                    reactMessage={props.likeMessage}
                                    toggleEditedMessage={props.toggleEditedMessage}
                                    deleteMessage={props.deleteMessage}
                                />
                                <MessageInput
                                    addMessage={props.addMessage}
                                />
                            </div>
                        )
                }
                {
                    editedMessage
                    && (
                        <EditMessage
                            message={editedMessage}
                            editMessage={props.editMessage}
                            toggleEditedMessage={props.toggleEditedMessage}
                        />
                    )
                }
            </div>
        );
    }
}

Chat.propTypes = {
    messages: PropTypes.arrayOf(PropTypes.object),
    loadMessages: PropTypes.func.isRequired,
    likeMessage: PropTypes.func.isRequired,
    toggleEditedMessage: PropTypes.func.isRequired,
    addMessage: PropTypes.func.isRequired,
    editMessage: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    isLoading: PropTypes.bool,
    editedMessage: PropTypes.objectOf(PropTypes.any),
    chatName: PropTypes.string.isRequired,
    currentUser: PropTypes.objectOf(PropTypes.any).isRequired,
};

Chat.defaultProps = {
    messages: [],
    isLoading: true,
    editedMessage: undefined,
};

const mapStateToProps = rootState => ({
    messages: rootState.chat.messages,
    editedMessage: rootState.chat.editedMessage,
    isLoading: rootState.chat.isLoading,
    chatName: rootState.chat.chatName,
    currentUser: rootState.chat.currentUser,
});

const actions = {
    loadMessages,
    likeMessage,
    toggleEditedMessage,
    addMessage,
    editMessage,
    deleteMessage,
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chat);