import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import chatReducer from './containers/Chat/reducer';
import thunk from 'redux-thunk';

const initialState = {
    chat: {
        chatName: 'Friends',
        currentUser: {
            id: '09023940234',
            user: 'Alex',
            avatar: 'https://i.pravatar.cc/300?img=14',
        }
    }
};

const reducers = {
    chat: chatReducer,
};

const middlewares = [
    thunk,
];

const composedEnhancers = compose(
    applyMiddleware(...middlewares)
);

const rootReducer = combineReducers({
    ...reducers
})

const store = createStore(
    rootReducer,
    initialState,
    composedEnhancers,
);

export default store;