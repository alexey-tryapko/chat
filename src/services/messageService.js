import callWebApi from './../helpers/webApiHelper';
import moment from 'moment';

const userID = "09023940234";

const generateId = () => (
    '' + Math.random().toString(36).substr(2, 9)
);

export const getAllMessages = async () => {
    const response = await callWebApi({
        endpoint: 'https://api.myjson.com/bins/1hiqin',
        type: 'GET',
    });
    return response.json();
};

export const getMessage = (id, messages) => {
    return messages.filter(message => message.id === id).shift();
}

export const addMessage = ({ body }, { user, avatar }) => {
    const newMessage = {
        id: generateId(),
        user,
        created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
        message: body,
        avatar,
        isCreator: true,
    }
    return newMessage;
};

export const likeMessage = (id, messages, curreentUserId) => {
    const index = messages.findIndex(message => message.id === id);
    const message = messages[index];
    let { likes } = message;
    if (likes) {
        const index = likes.indexOf(userID);
        if (index !== -1) {
            likes = likes.filter(id => id !== userID);
        } else {
            likes.push(curreentUserId);
        }
    } else {
        likes = [curreentUserId];
    }
    message.likes = likes;
    return message;
};

export const deleteMessage = id => {
    return true;
};

export const editMessage = (updatedMessage) => {
    return updatedMessage;
};
